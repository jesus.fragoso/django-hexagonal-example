FROM tiangolo/uvicorn-gunicorn-fastapi:latest

ENV PYTHONUNBUFFERED=1

WORKDIR /app

RUN pip3 install --upgrade pip==22.1.1

RUN pip3 install poetry

COPY poetry.lock pyproject.toml /app/

RUN poetry config virtualenvs.create false \
    && poetry install --no-dev

COPY src/ /app/

EXPOSE 5677

CMD python3 -m debugpy --listen 0.0.0.0:5677 /usr/local/bin/gunicorn --chdir /app deployment.wsgi:application --bind 0.0.0.0:7760 --timeout 300 --keep-alive 300
