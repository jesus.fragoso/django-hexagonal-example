# Create **`requirements.txt`** file

<br>

```console
pip3 list --format=freeze > requirements.txt
```


Generate the **`requirements.txt`** file with **`poetry`** :
```console
poetry export -f requirements.txt -o requirements.txt --without-hashes --dev
```
