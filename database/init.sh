#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE ROLE fragoso WITH ENCRYPTED PASSWORD 'mR\W8=FMSV=ukuMJ';

    ALTER ROLE "fragoso" WITH LOGIN;

    ALTER USER fragoso WITH SUPERUSER;

    CREATE DATABASE fragoso;

    GRANT ALL PRIVILEGES ON DATABASE fragoso TO fragoso;

    GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public to fragoso;

    \connect fragoso;

    CREATE TABLE users(
    id BIGSERIAL NOT NULL,
    name VARCHAR(250) NULL,
    age INTEGER NULL,
    country VARCHAR(250) NULL,
    CONSTRAINT users_pk PRIMARY KEY (id)
    );

    CREATE TABLE customers(
        id BIGSERIAL NOT NULL,
        contact_name VARCHAR(250) NULL,
        country VARCHAR(250) NULL,
        CONSTRAINT customers_pk PRIMARY KEY (id)
    );

    CREATE TABLE orders(
        id BIGSERIAL NOT NULL,
        ship_name VARCHAR(250) NULL,
        customer_id INT8 NULL,
        CONSTRAINT orders_pk PRIMARY KEY (id)
    );

    CREATE INDEX orders_cusromer_id_index ON orders USING btree(customer_id);

    ALTER TABLE orders ADD CONSTRAINT orders_customers_fk FOREIGN KEY (customer_id) REFERENCES customers (id);

EOSQL
