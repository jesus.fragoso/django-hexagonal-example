from .base import *

DEBUG = env.bool("DEBUG")

THIRD_PARTY_APPS = [
    "rest_framework"
]

PROJECT_APPS = [
    "services.apps.users",
    "services.apps.orders",

]

INSTALLED_APPS = BASE_APPS + THIRD_PARTY_APPS + PROJECT_APPS

THIRD_PARTY_MIDDLEWARES = [
    'django_structlog.middlewares.RequestMiddleware',
]

MIDDLEWARE = BASE_MIDDLEWARES + THIRD_PARTY_MIDDLEWARES

WSGI_APPLICATION = 'deployment.wsgi.application'

DATABASES: Dict = {}

DATABASES["default"] = env.db_url("DATABASE_URL")
DATABASES["default"]["ATOMIC_REQUESTS"] = True

REST_FRAMEWORK = {
    "EXCEPTION_HANDLER": "utils.handlers.exception_response",
}
