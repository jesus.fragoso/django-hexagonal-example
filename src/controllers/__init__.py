class BaseController:
    @classmethod
    def process_data(cls, *args, **kwargs):
        method_obtained = cls._obtain_method()
        data_processed = method_obtained(*args, **kwargs)
        return data_processed

    @classmethod
    def _obtain_method(cls):
        method = getattr(cls.commands, cls.method_name)
        return method
