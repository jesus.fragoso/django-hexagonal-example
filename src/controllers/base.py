from rest_framework.response import Response
from utils.decorators import validator
from controllers import BaseController

class GetterController(BaseController):
    def __call__(self, request, *args, **kwargs):
        data_obtained = self.process_data(request, *args, **kwargs)

        data_serializer = self.serializer(data_obtained, many=True)
        return Response({self.response_data_name: data_serializer.data})


class CreatorController(BaseController):
    @validator
    def __call__(self, request, *args, **kwargs):
        data_created = self.process_data(request, *args, **kwargs)

        data_serializer = self.serializer(data_created)
        return Response({self.response_data_name: data_serializer.data})
