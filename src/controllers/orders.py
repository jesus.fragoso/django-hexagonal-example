from controllers.base import GetterController
from controllers.base import CreatorController
from domain.commands.orders import CustomerCommands
from presenters.orders import CustomerSerializer

class CustomerController(GetterController):
    commands = CustomerCommands()
    serializer = CustomerSerializer
    method_name = "get_all_customers"
    response_data_name = "customers"


class CustomerCreatorController(CreatorController):
    pass
