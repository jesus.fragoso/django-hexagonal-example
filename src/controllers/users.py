from domain.commands.users import UserCommands
from presenters.users import UserSerializer

from controllers.base import CreatorController
from controllers.base import GetterController


class UserController(GetterController):
    commands = UserCommands()
    serializer = UserSerializer
    method_name = "get_all_users"
    response_data_name = "users"

class UserCreatorController(CreatorController):
    commands = UserCommands()
    serializer = UserSerializer
    method_name = "create_user"
    response_data_name = "user"
