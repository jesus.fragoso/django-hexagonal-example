from .base import CustomException

class DjangoImportException(CustomException):
    message = (
        "Couldn't import Django. Are you sure it's installed and "
        "available on your PYTHONPATH environment variable? Did you "
        "forget to activate a virtual environment?"
    )

    def __init__(self):
        super().__init__(self.message)
