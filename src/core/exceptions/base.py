from rest_framework.exceptions import APIException

class CustomException(APIException):
    pass
