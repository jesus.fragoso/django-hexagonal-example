from rest_framework.exceptions import APIException

class CustomersNotFound(APIException):
    default_detail = "There aren't customers inside the database"
    code = 404
