from .base import CustomException
from rest_framework.exceptions import APIException


class UsersNotFound(APIException):
    default_detail = "There aren't users inside the database"
    code = 404


class UserNotCreated(CustomException):
    message = "The user wasn't created"
    code = 404
