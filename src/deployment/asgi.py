import os

from django.core.asgi import get_asgi_application
from dotenv import load_dotenv
from dotenv import find_dotenv

load_dotenv(find_dotenv())

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.base')

application = get_asgi_application()
