from domain.repositories.repository import DjangoRepository
from domain.models import User
from domain.models import Customer


class DjangoAdapter:

    @property
    def user(self):
        return DjangoRepository(User)

    @property
    def customer(self):
        return DjangoRepository(Customer)
