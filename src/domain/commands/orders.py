from domain.commands.base import BaseCommand
from core.exceptions.orders import CustomersNotFound


class CustomerCommands(BaseCommand):
    def get_all_customers(self, *args, **kwargs):
        queryset = self.adapter.customer.get_all()

        if self._count_customers(queryset) == 0:
            raise CustomersNotFound

        return queryset

    def _count_customers(self, queryset) -> int:
        return queryset.count()

    def create_customer(self, *args, **kwargs):
        return self.adapter.customer.create(**kwargs)
