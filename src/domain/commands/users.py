from domain.commands.base import BaseCommand
from core.exceptions.users import UsersNotFound

class UserCommands(BaseCommand):
    def get_all_users(self, *args, **kwargs):
        queryset = self.adapter.user.get_all()

        if self._count_users(queryset) == 0:
            raise UsersNotFound
        return queryset

    def _count_users(self, queryset) -> int:
        return queryset.count()

    def create_user(self, *args, **kwargs):
        return self.adapter.user.create(**kwargs)
