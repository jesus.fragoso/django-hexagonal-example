from pydantic import dataclasses


@dataclasses.dataclass
class Dog:
    name: str
    age: int



if __name__ == '__console__':
    dog_data = {
        "name": "Bucky",
        "age": 4
    }

    dog_instance = Dog(**dog_data)
P
