from django.db import models

class Customer(models.Model):
    contact_name = models.CharField(max_length=250, null=True)
    country = models.CharField(max_length=250, null=True)

    class Meta:
        db_table = "customers"
        app_label = "services.apps.orders"
