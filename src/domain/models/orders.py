from django.db import models
from .customers import Customer

class Order(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True)
    ship_name = models.CharField(max_length=250, null=True)

    class Meta:
        db_table = "orders"
        app_label = "services.apps.orders"
