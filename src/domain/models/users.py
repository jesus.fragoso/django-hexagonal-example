from django.db import models

class User(models.Model):
    name = models.CharField(max_length=250, null=True)
    age = models.IntegerField(null=True)
    country = models.CharField(max_length=250, null=True)

    class Meta:
        db_table = "users"
        app_label = "services.apps.users"


class Phones(models.Model):

    class Meta:
        db_table = "phones"
        app_label = "services.apps.users"
