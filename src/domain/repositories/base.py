import abc

class RepositoryInterface(abc.ABC):

    @abc.abstractmethod
    def get_all(self, **kwargs):
        raise NotImplementedError

    @abc.abstractmethod
    def get_one(self, **kwargs):
        raise NotImplementedError