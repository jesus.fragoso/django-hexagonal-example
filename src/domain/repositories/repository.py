from dataclasses import dataclass
from typing import Dict
from typing import Tuple

from django.db import models

from .base import RepositoryInterface


@dataclass
class DjangoRepository(RepositoryInterface):

    model: models.Model

    def get_all(self, **kwargs: Dict):
        return self.model.objects.all()

    def get_one(self, **kwargs: Dict):
        return self.model.objects.get(**kwargs)

    def create(self, *args, **kwargs: Dict):
        return self.model.objects.create(**kwargs)
