import sys
from typing import NoReturn

from dotenv import find_dotenv
from dotenv import load_dotenv

from core.exceptions.admin import DjangoImportException


class Entrypoint:

    def start(self):
        self.load_configuration()
        self.read_arguments()

    def load_configuration(self) -> NoReturn:
        load_dotenv(find_dotenv())

    def read_arguments(self) -> NoReturn:
        try:
            from django.core import management
        except ImportError as exception:
            raise DjangoImportException from exception

        management.execute_from_command_line(sys.argv)


if __name__ == '__main__':
    entrypoint = Entrypoint()

    entrypoint.start()
