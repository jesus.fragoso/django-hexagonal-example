from rest_framework import serializers
from domain.models.users import User
from domain.models.users import Phones



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class Phones(serializers.ModelSerializer):

    class Meta:
        model = Phones
        fields = '__all__'
