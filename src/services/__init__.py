from typing import Dict
from typing import Tuple

from rest_framework import generics


class ListService(generics.ListAPIView):
    def list(self, request, *args: Tuple, **kwargs: Dict) -> Dict:
        return self.list_controller(request, *args, **kwargs)


class RetrieveService(generics.RetrieveAPIView):
    def retrieve(self, request, *args: Tuple, **kwargs: Dict) -> Dict:
        return self.retrieve_controller(request, *args, **kwargs)


class CreateService(generics.CreateAPIView):
    def create(self, request, *args: Tuple, **kwargs: Dict) -> Dict:
        return self.create_controller(request, *args, **kwargs)


class UpdateService(generics.UpdateAPIView):
    def update(self, request, *args: Tuple, **kwargs: Dict) -> Dict:
        return self.update_controller(request, *args, **kwargs)
