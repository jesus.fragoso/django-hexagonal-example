from services import ListService
from services import CreateService

from controllers.orders import CustomerController


class CustomerList(ListService, CreateService):
    list_controller = CustomerController()
    create_controller = ...
