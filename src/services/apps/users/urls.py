from .views import UserList
from django.urls import path

urlpatterns = [
    path("", UserList.as_view())
]