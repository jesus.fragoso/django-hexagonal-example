from services import ListService
from services import CreateService
from controllers.users import UserController
from controllers.users import UserCreatorController

class UserList(ListService, CreateService):
    list_controller = UserController()
    create_controller = UserCreatorController()
