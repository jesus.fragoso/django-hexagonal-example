from pytest import fixture
from deployment.wsgi import application
import pytest
from django.db import connection


@pytest.fixture(scope='session')
def django_db_setup(django_db_blocker):
    with django_db_blocker.unblock():
        with connection.cursor() as c:
            c.execute('''
            CREATE TABLE "users" (
                "id" integer NOT NULL PRIMARY KEY,
                "name" VARCHAR(250),
                "age" INTEGER
            );

            CREATE TABLE "customers" (
                "id" integer NOT NULL PRIMARY KEY,
                "contact_name" varchar(30) NULL);
            ''')


@pytest.mark.django_db
def django_db_setup (django_db_blocker):

    with connection.cursor() as cursor:
        cursor.executescript("""

        """)

@pytest.mark.django_db
@fixture
def user_data():
    return {
        "name": "Boromir",
        "age": 32,
    }
