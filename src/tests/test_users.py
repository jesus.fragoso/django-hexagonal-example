import pytest
from controllers.users import UserController
from controllers.users import UserCreatorController
from core.exceptions.users import UsersNotFound
from typing import Dict
from typing import Tuple


@pytest.mark.django_db
class TestUsers:
    getter_controller = UserController()
    creator_controller = UserCreatorController()

    def test_users_not_found(self, request, *args: Tuple, **kwargs: Dict):
        with pytest.raises(UsersNotFound):
            self.getter_controller(request)


    def test_create_user(self, request, *args: Tuple, user_data, **kwargs: Dict):
        setattr(request, "data", user_data)
        user_created = self.creator_controller(request)

        assert user_created.status_code == 200
        assert isinstance(user_created.data, dict)
        assert "user" in user_created.data.keys()
        assert isinstance(user_created.data["user"], dict)


    def test_get_all_users(self, request, *args: Tuple, user_data, **kwargs: Dict):
        setattr(request, "data", user_data)

        user_created = self.creator_controller(request)
        user_created = self.creator_controller(request)

        users_obtained = self.getter_controller(request)

        assert users_obtained.status_code == 200
        assert isinstance(users_obtained.data, dict)
        assert "users" in users_obtained.data.keys()
        assert isinstance(users_obtained.data["users"], list)
