from rest_framework import serializers
from rest_framework.exceptions import APIException
from core.exceptions import EXCEPTIONS
from typing import Callable
from typing import Tuple
from typing import Dict


def validator(main_function: Callable) -> Callable:
    def wrapper(self, request, *args: Tuple, **kwargs: Dict):
        data_serializer:serializers.Serializer = self.serializer(data=request.data)
        data_serializer.is_valid(raise_exception=True)

        return main_function(self, request, *args, **data_serializer.data,  **kwargs)
    return wrapper
