from rest_framework.views import exception_handler


def exception_response(exception, context):
    response = exception_handler(exception, context)

    if "detail" in response.data.keys():
        response.data["error_message"] = response.data.pop("detail")

    return response
